FROM node:lts-alpine3.17

EXPOSE 3000

WORKDIR /app/website

# COPY ./website/package.json /app/website/package.json
COPY ./website /app/website
# If needed, setup proxy using .yarnrc file
COPY .yarnrc /app/website/.yarnrc
RUN yarn install

CMD ["yarn", "start", "--host", "0.0.0.0"]
