## Sylva documentation project

This project collects markdown file documentations related to the Sylva project, 
renders them using [docusaurus](https://docusaurus.io/) framework 
and hosts them using [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) 

The `master` branch of this project is built by the pipeline, 
and accessible on the URL http://sylva-projects.gitlab.io

## Project folder structure

The documentation files are inside the `docs` folder.

Other files and folder of the project are organized as follows:

```
.
├── docker-compose.yml              # config for docker compose 
├── Dockerfile                      # used to build local image run by compose
├── docs                            # main folder for contribution
│   ├── installation-guide.md
│   ├── design.md
│   └── ...
├── .yarnrc-example                 # example file to create your own .yarnrc for proxy
├── README.md
└── website                         # website configuration and static content
```

Please note that the content inside `website` are not meant to be changed frequently.
They are copied inside the docker image, so any change inside this folder requires to rebuild the image.


## How to contribute to the documentation

1. Write the documentation using markdown syntax

Doc files are in `docs` folder.  
They should be written using markdown syntax. 
Some additional markdown features are available in Docusaurus.  
They are explained here https://docusaurus.io/docs/markdown-features

2. Build and start the local container

to build and test the documentation website locally

    docker compose up

This will build a docker image called `sylva-docusaurus`, install the dependencies and start a container named `sylva-local-preview` to render the local docs.

:warning: the "sylva-docusaurus" docker image has a large size (>2GB), because all the packages installed.

Some proxy configuration for `yarn` may be needed, to download/install packages from Internet repos.  

If you are behind a web proxy, the proxy address and port should be set in `.yarnrc` file. (an example file is provided as `.yarnrc-example`)

3. Preview and edit the docs

The local server is accessible at http://localhost:3000.
It provides the **hot** preview for all markdown files in the `docs` folder, i.e.
The changes in the markdown file are rendered immediately by the server without restart.

4. Clean up

If you want stop the container, you can run

    docker compose rm

or, since the container has a static name

    docker rm sylva-local-preview

If you have modified anything inside the `website` folder, and you want to rebuild the image

    docker compose up --build

If you want to remove everything and start over

    docker rm sylva-local-preview
    docker rmi sylva-docusaurus
